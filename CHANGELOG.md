# Version 1.0.1
- Print and check validity of command line arguments and configuration variables
- Retrieve standard output and error of _all_ Hadoop jobs, not just the first one
- Run Spark applications
- Set Spark deploy mode and any other `spark-submit` option
- Retrieve output of the `spark-submit` command

# Version 1.0.0
- Run Hadoop applications
- Use input data from your machine or from the cluster's HDFS
- Retrieve merged output data
- Retrieve output of the `hadoop` command
- Detect `.jar` file and driver class automatically
- Log in to the cluster non-interactively
- Retrieve application standard output and error
- Sort output data
