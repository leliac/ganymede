# Ganymede
<img src="icon.png" width="200">

### Author
Gabriele Cazzato (gabriele.cazzato@studenti.polito.it)

_Enforced equality of outcome is simply authoritarianism; when under the guise of pretty names like "diversity", "inclusivity" or "anti-\<someBadWord>" it is authoritarianism with a smile. Defend and uphold equality of opportunity, liberalism, meritocracy and freedom of speech at your university, at your workplace, on the Internet and in all institutions. Conserve what is good about the West, or you'll fail miserably at making it better._

_Dedicated to Azami. If this program frees me to spend only a few more minutes per week with you, I'll feel like a better man._

### Target audience
Students and teachers of the course [_Big Data: Architectures and Data Analytics_](https://dbdmg.polito.it/wordpress/teaching/big-data-architectures-and-data-analytics-2020-2021/) held at Polytechnic University of Turin; more generally, people with authorized access to the [_BigData@Polito_](https://smartdata.polito.it/computing-facilities/) cluster. Please make sure to understand how the script works before using it and then use it with care.

### Description
Execute your Hadoop or Spark applications on the _BigData@Polito_ cluster with input data from your machine or from the cluster's HDFS, retrieving output data and output of the `hadoop` or `spark-submit` command. Additional features:
- detect `.jar` file and driver class automatically
- log in to the cluster non-interactively
- retrieve standard output and error
- sort output data

### Usage
1. Adjust your application's driver in order to process the arguments string `<hdfsInputPath1>[ <hdfsInputPath2> ...] <hdfsOutputDir> [<otherArgs>]` and re-build the `.jar` file
2. Set the configuration variables appropriately
3. Run as `./ganymede <inputPath1>[,<inputPath2>,...] <localOutputDir> [<otherArgs>]`

### Configuration variables

| Name | Explanation |
| ------ | ------ |
| `remoteInputPaths` | if `0`, `<inputPath1>[,<inputPath2>,...]` are interpreted as local paths; if `1`, as paths on the cluster's HDFS |
| `jarFile` | if empty, guess from `./pom.xml` |
| `driverClass` | if empty, guess from the first file matching `*driver*.class` (case insensitive) inside `./target/classes` |
| `clusterUsername` | must be set to you [bigdatalab.polito.it](https://bigdatalab.polito.it) username |
| `clusterPassword` | if not empty, log in to the cluster non-interactively (requires `sshpass` installed) |
| `framework` | `hadoop` or `spark` |
| `sparkDeployMode` | `cluster` or `client`; only relevant if `framework` is `spark` |
| `otherSparkOptions` | `spark-submit` options, except `--deploy-mode`, `--master` and `--class`; only relevant if `framework` is `spark` |
| `getStdOutErr` | if `1`, retrieve your application's standard output and error |
| `sortOptions` | if not empty, sort output data locally with `sort $sortOptions` |
